Project built using AngularJS and Spring Boot to demostrate the web application.

Used items:-

1. Spring boot
2. Spring Data JPA
3. AspectJ Logging
4. YAML Configuration
5. Spring Profiles
6. H2 db/MySQL
7. AngularJS 

Using the default logback logging provided by Spring Boot

How to run:-

Import the project into your STS(Spring Tool Suite) workspace. If you are using eclipse, make sure to download the STS plugin.
Make sure to change the context-path and port as per your need. Run as Spring Boot App or any other web container as per ypur choice.


You can see the app in live action at:- https://xlyps.cfapps.io/
