package com.xlyps.employee.details.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlyps.employee.details.domain.Loan;
import com.xlyps.employee.details.exception.ServiceException;
import com.xlyps.employee.details.repository.LoanRepository;

/**
 * The Class LoanServiceImpl.
 */
@Service
public class LoanServiceImpl implements LoanService {

	/** The loan repository. */
	@Autowired
	private LoanRepository loanRepository;	

	public boolean isEligibleForLoan(int empId) throws ServiceException {
		Loan loan=loanRepository.findByEmpId(empId);
		if(null!=loan) {
			if(!loan.getStatus().equalsIgnoreCase("pending"))
				return true;
			else
				return false;
		}
		else
			return true;
		
	}
}
