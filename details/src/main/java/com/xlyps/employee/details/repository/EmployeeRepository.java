package com.xlyps.employee.details.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xlyps.employee.details.domain.Employee;

/**
 * The Interface EmployeeRepository.
 */
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {	
	
}
