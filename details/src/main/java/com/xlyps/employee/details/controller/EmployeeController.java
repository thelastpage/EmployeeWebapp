package com.xlyps.employee.details.controller;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xlyps.employee.details.domain.Employee;
import com.xlyps.employee.details.exception.ServiceException;
import com.xlyps.employee.details.service.EmployeeService;

@RestController  
public class EmployeeController  {  

	@Autowired
	private EmployeeService empService;

	@GetMapping("/employees")
	public List<Employee> getAllEmployees() {
		return empService.getAllEmployees();
	}
	
	@PostMapping("/addEmployee")
	public List<Employee> addEmployee(@RequestBody Employee emp) {
		try {
			empService.createEmployee(emp);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return getAllEmployees();
	}
	
	@PutMapping("/updateEmployee")
	public List<Employee> updateEmployee(@RequestBody Employee emp) {
		empService.updateEmployee(emp);
		return getAllEmployees();
	}
	
	@DeleteMapping("/delete/{id}")
	public List<Employee> deleteEmployee(@PathVariable("id") int id) {
		return empService.deleteEmployee(id);
	}

} 
