package com.xlyps.employee.details.service;

import java.sql.SQLException;
import java.util.List;

import com.xlyps.employee.details.domain.Employee;
import com.xlyps.employee.details.exception.ServiceException;

/**
 * The Interface EmployeeService.
 */
public interface EmployeeService {

	/**
	 * Insert employee.
	 *
	 * @param employee
	 *            the employee
	 * @throws SQLException
	 *             the SQL exception
	 * @throws ServiceException
	 *             the service exception
	 */
	// Method to access data access layer method to insert Employee record
	public void createEmployee(Employee employee) throws SQLException, ServiceException;

	/**
	 * Checks if is eligible for loan.
	 *
	 * @param empId
	 *            the emp id
	 * @return true, if is eligible for loan
	 * @throws ServiceException
	 *             the service exception
	 */
	public boolean isEligibleForLoan(int empId) throws ServiceException;

	/**
	 * Gets the employee details.
	 *
	 * @param empId
	 *            the emp id
	 * @return the employee details
	 * @throws ServiceException
	 *             the service exception
	 */
	public Employee getEmployeeDetails(int empId) throws ServiceException;

	public List<Employee> getAllEmployees();

	public List<Employee> deleteEmployee(int id);

	public void updateEmployee(Employee emp);

	

}
