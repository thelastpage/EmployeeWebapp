package com.xlyps.employee.details.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xlyps.employee.details.domain.Loan;

/**
 * The Interface LoanRepository.
 */
public interface LoanRepository extends JpaRepository<Loan, Integer> {
	Loan findByEmpId(int empId);
	
}
