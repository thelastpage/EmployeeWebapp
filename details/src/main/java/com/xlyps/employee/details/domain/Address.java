package com.xlyps.employee.details.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class Address.
 */
@Entity
@Table(name="address")
public class Address {
	
	@Id
    @Column(name="empId")
	@GeneratedValue
	private int addrId;
	
	public int getAddrId() {
		return addrId;
	}

	public void setAddrId(int addrId) {
		this.addrId = addrId;
	}			

	/** The address deatils. */
	private String addressDeatils;

	/** The state. */
	private String state;

	/** The pincode. */
	private int pincode;

	/**
	 * Gets the address deatils.
	 *
	 * @return the address deatils
	 */
	public String getAddressDeatils() {
		return addressDeatils;
	}

	/**
	 * Sets the address deatils.
	 *
	 * @param addressDeatils
	 *            the new address deatils
	 */
	public void setAddressDeatils(String addressDeatils) {
		this.addressDeatils = addressDeatils;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state
	 *            the new state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the pincode.
	 *
	 * @return the pincode
	 */
	public int getPincode() {
		return pincode;
	}

	/**
	 * Sets the pincode.
	 *
	 * @param pincode
	 *            the new pincode
	 */
	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	/**
	 * Instantiates a new address.
	 */
	public Address() {
		super();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Address [addressDeatils=" + addressDeatils + ", state=" + state + ", pincode=" + pincode + "]";
	}

}
