package com.xlyps.employee.details.service;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlyps.employee.details.domain.Employee;
import com.xlyps.employee.details.exception.ServiceException;
import com.xlyps.employee.details.repository.EmployeeRepository;

/**
 * The Class EmployeeServiceImpl.
 */

@Service
public class EmployeeServiceImpl implements EmployeeService {

	/** The employee repository. */
	@Autowired
	private EmployeeRepository empRepo;

	@Autowired
	private LoanService loanService;

	public void createEmployee(Employee employee) throws SQLException, ServiceException {

		empRepo.saveAndFlush(employee);
	}

	public Employee getEmployeeDetails(int empId) throws ServiceException {
		Employee e= empRepo.findOne(empId);
		if(e==null)
			throw new ServiceException("No such employee exists!!");
		e.getEmpId();
		return e;
	}


	public boolean isEligibleForLoan(int empId) throws ServiceException {
		boolean ret = hasRoleEligibility(empId);
		if (ret) {			
			ret = loanService.isEligibleForLoan(empId);
		}
		return ret;
	}

	/**
	 * Checks for role eligibility.
	 *
	 * @param empId
	 *            the emp id
	 * @return true, if successful
	 * @throws ServiceException
	 *             the service exception
	 */
	private boolean hasRoleEligibility(int empId) throws ServiceException {

		String role = null;
		boolean result = true;
		Employee employee = empRepo.findOne(empId);
		if (employee == null) {
			throw new ServiceException("No record found for the given employee details");
		} else {
			role = employee.getRole();
			if (role == null || role.equalsIgnoreCase("Manager") || role.equalsIgnoreCase("GM")) {
				result = false;
			} else {
				result = true;
			}
		}
		return result;
	}

	public List<Employee> getAllEmployees() {
			List<Employee> list=empRepo.findAll();
			return list;
	}

	public List<Employee> deleteEmployee(int id) {
		empRepo.delete(id);
		return getAllEmployees();
	}

	public void updateEmployee(Employee emp) {
		empRepo.saveAndFlush(emp);		
	}	
}
