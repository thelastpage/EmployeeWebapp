angular.module('myApp').controller("homeController", function($scope, $http, $location,empService) {

	$scope.employeeData=[];
	$scope.showModal=false;
	
	$http({
		method: 'GET',
		url: '/Xylps/employees',
		headers: {
			"Content-Type": "application/json"
		}
	}).then(function(response) {
		$scope.employeeData=response.data;
		empService.setObject($scope.employeeData[0]);
		//console.log(empService.getObject());
	},function(response){
		//alert("Error");
	});
	
	$scope.updateEmployee=function(index){
		
		empService.setObject($scope.employeeData[index]);
		empService.setFlag(true);
		$location.path("/updateEmployee" );
	};


	$scope.deleteEmployee=function(index){
		var empId=$scope.employeeData[index].empId;
		$http({
			method: 'DELETE',
			url: '/Xylps/delete/'+empId,
			headers: {
				"Content-Type": "application/json"
			}
		}).then(function(response) {
			$scope.employeeData=response.data;
		},function(response){
			//alert("Error");
		});
	};
	
	
	$scope.showEmployeeModal=function(){
	    $location.path("/addEmployee" );
	};
});
