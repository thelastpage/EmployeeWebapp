angular.module('myApp').controller("employeeController", function($scope, $http,empService) {	
	
	//$scope.employeeData=empService.getObject();
	$scope.employee={};
	$scope.userOperation="Register";
	if(empService.getFlag()==true){
		$scope.employee=empService.getObject();
		empService.setFlag(false);
		$scope.userOperation="Update";
	}
	
	
	$scope.addEmployee=function(data){
		$http({
			method: 'POST',
			url: '/Xylps/addEmployee',
			data: $scope.employee,
			headers: {
				"Content-Type": "application/json"
			}
		}).then(function(response) {
			$scope.employeeData=response.data;
			$location.path("/home" );
		},function(response){
			//alert("Employee couldn't be added");
		});
	};
	
	$scope.updateEmployee=function(data){
		$http({
			method: 'PUT',
			url: '/Xylps/updateEmployee',
			data: $scope.employee,
			headers: {
				"Content-Type": "application/json"
			}
		}).then(function(response) {
			$scope.employeeData=response.data;
			$location.path("/home" );
		},function(response){
			//alert("Employee couldn't be added");
		});
	};
});
