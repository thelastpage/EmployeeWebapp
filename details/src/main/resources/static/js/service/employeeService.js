angular.module('myApp').service('empService', function() {
    var employee = {};
    var updateFlag=false;
    
    return {
        getObject: function() {
            return employee;
        },setObject: function(value) {
            employee = value;
        },  getFlag: function() {
            return updateFlag;
        },setFlag: function(value) {
        	updateFlag = value;
        }
    }
});