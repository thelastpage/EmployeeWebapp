'use strict';
 
var App = angular.module('myApp',['ngRoute']);
 
App.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/home', {
            templateUrl: './pages/home.html',
            controller : "homeController",
        })
        .when('/addEmployee',{
        	templateUrl:'./pages/addEmployee.html',
            controller : "employeeController",
        })
        .when('/updateEmployee',{
        	templateUrl:'./pages/addEmployee.html',
            controller : "employeeController",
        })
        .otherwise({redirectTo:'/home'});        
}]);